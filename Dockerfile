FROM python:3

COPY . /tmp

WORKDIR /tmp 

# Set the env variables to non-interactive
ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes

# install latex packages
RUN apt-get update -y \
  && apt-get install -y --no-install-recommends \
    pandoc

# 
#RUN pip install pandoc-dalibo-guidelines

RUN pandoc --filter ./pandoc_extract_code.py \ 
           ./pandoc_extract_code.sample.md \ 
           -o ./pandoc_extract_code.sample.html
