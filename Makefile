SAMPLE?=./pandoc_extract_code.sample.md
FILTER?=./pandoc_extract_code.py
DIST?=dist

sdist: 
	python setup.py sdist

install: 
	python setup.py install	

test: basic_test pandoc_extract_code.sample.html

basic_test:
	echo 'hello world' | pandoc -t json | python -tt $(FILTER)
 
%.html: %.md
	pandoc --filter $(FILTER) $^ -o $@

%.pdf:  %.md
	pandoc --filter $(FILTER) $(SAMPLE) -o $(SAMPLE).pdf


testpypi: clean sdist 
	twine upload --skip-existing --config-file ./.pypirc -r testpypi $(DIST)/*
	pip install --user -i https://testpypi.python.org/pypi --extra-index-url https://pypi.org/simple pandoc-extract-code
	pip uninstall --yes pandoc-extract-code
	
pypi: clean sdist
	twine upload --skip-existing $(DIST)/*

docker: 
	docker build .

clean:
	rm -f *.pdf *.tex
	rm -fr $(DIST)
