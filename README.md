# pandoc-extract-code 

A pandoc filter that creates a separate file for each code block in a markdown document.


## See Also 

* panpipe:  https://hackage.haskell.org/package/panpipe