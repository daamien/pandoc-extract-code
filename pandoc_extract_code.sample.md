# 

## Test script python

``` python
test='ceci est un test'
print test
```

another code_block

```yaml
receipt:     Oz-Ware Purchase Invoice
date:        2012-08-06
customer:
    given:   Dorothy
    family:  Gale
```


## Test mermaid


``` {filename="plop.mmd"  .mermaid }
graph TD
  app> fa:fa-cloud Application]
  pool( fa:fa-filter pgBouncer)
  1[ fa:fa-database Node A = Postgres Primary]
  2[ fa:fa-database Node B = Postgres Standby]
  3[ fa:fa-database Node C = Postgres Standby ]
  app--SQL-->pool
  pool--SQL-->1
  pool-.fallback.->2
  1--synchonous replication-->2
  1--asynchronous replication-->3
```

## Test unknown language

``` whatever_language_that_does_not_exist 
This should be exported as `.md` file
```

## Test without no language specified

```
This should not be exported
```
